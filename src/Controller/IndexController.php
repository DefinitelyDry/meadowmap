<?php
// src/Controller/IndexController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Marker;
use App\Entity\MarkerType;

class IndexController extends Controller {
    public function index() {
    	$markers = $this->getDoctrine()->getRepository(Marker::class)->findAll();
    	$markerTypes = $this->getDoctrine()->getRepository(MarkerType::class)->findAll();

        return $this->render('index.html.twig', ['markers' => $markers, 'markerTypes' => $markerTypes]);
    }

    public function privacy() {
    	return $this->render('privacy.html.twig');
    }

    public function imprint() {
    	return $this->render('imprint.html.twig');
    }
} 
