<?php
// src/Controller/AdminController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Marker;
use App\Entity\MarkerType;

class AdminController extends Controller {
    public function index() {
    	$this->denyAccessUnlessGranted('ROLE_ADMIN');
    	$markerTypes = $this->getDoctrine()->getRepository(MarkerType::class)->findAll();

        return $this->render('admin.html.twig', ['markerTypes' => $markerTypes]);
    }

    public function new(Request $request) {
    	// Check if Marker has already been set
    	$em = $this->getDoctrine()->getManager();
    	$type = $this->getDoctrine()->getRepository(MarkerType::class)->find($request->request->get('type'));
    	$x = $request->request->get('x');
    	$z = $request->request->get('z');
    	$t = 10; // Tolerance
    	$qb = $em->createQueryBuilder();
		$q  = $qb->select(array('m'))
		->from('App:Marker', 'm')
		->where('m.type = ?0')
		->andWhere('m.x > ?1')
		->andWhere('m.x < ?2')
		->andWhere('m.z > ?3')
		->andWhere('m.z < ?4')
		->setParameter(0, $type)
		->setParameter(1, $x - $t)
		->setParameter(2, $x + $t)
		->setParameter(3, $z - $t)
		->setParameter(4, $z + $t)
		->getQuery();

		if($q->getOneOrNullResult()) {
			$this->addFlash('error', 'Marker already exists at '.$q->getSingleResult()->getX().'/'.$q->getSingleResult()->getZ());
			return $this->redirectToRoute('admin');
		}

    	// Create new Marker
    	$marker = new Marker();
    	$marker->setType($type);
    	$marker->setTitle($request->request->get('title'));
    	$marker->setDescription($request->request->get('description'));
    	$marker->setImage($request->request->get('image'));
    	$marker->setImageAuthor($request->request->get('image_author'));
    	$marker->setX($x);
    	$marker->setZ($z);
    	$marker->setIcon($request->request->get('icon'));
    	$marker->setIconWidth($request->request->get('icon_width') != "" ? $request->request->get('icon_width') : null);
    	$marker->setIconHeight($request->request->get('icon_height') != "" ? $request->request->get('icon_height') : null);
    	$marker->setIconAnchor($request->request->get('icon_anchor') != "" ? $request->request->get('icon_anchor') : null);
    	$marker->setUnderground($request->request->get('underground') ? 1 : 0);
    	$em->persist($marker);
    	$em->flush();

    	$this->addFlash('success', "Marker successfully created!");
    	return $this->redirectToRoute('admin');
    }
} 
