<?php

namespace App\Entity;

use App\Repository\MarkerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MarkerRepository::class)
 */
class Marker
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $x;

    /**
     * @ORM\Column(type="integer")
     */
    private $z;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=MarkerType::class, inversedBy="markers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $icon_width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $icon_height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $icon_anchor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_author;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $underground;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getX(): ?int
    {
        return $this->x;
    }

    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function getZ(): ?int
    {
        return $this->z;
    }

    public function setZ(int $z): self
    {
        $this->z = $z;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getType(): ?MarkerType
    {
        return $this->type;
    }

    public function setType(?MarkerType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getIconWidth(): ?int
    {
        return $this->icon_width;
    }

    public function setIconWidth(?int $icon_width): self
    {
        $this->icon_width = $icon_width;

        return $this;
    }

    public function getIconHeight(): ?int
    {
        return $this->icon_height;
    }

    public function setIconHeight(?int $icon_height): self
    {
        $this->icon_height = $icon_height;

        return $this;
    }

    public function getIconAnchor(): ?int
    {
        return $this->icon_anchor;
    }

    public function setIconAnchor(?int $icon_anchor): self
    {
        $this->icon_anchor = $icon_anchor;

        return $this;
    }

    public function getImageAuthor(): ?string
    {
        return $this->image_author;
    }

    public function setImageAuthor(?string $image_author): self
    {
        $this->image_author = $image_author;

        return $this;
    }

    public function getUnderground(): ?int
    {
        return $this->underground;
    }

    public function setUnderground(?int $underground): self
    {
        $this->underground = $underground;

        return $this;
    }
}
