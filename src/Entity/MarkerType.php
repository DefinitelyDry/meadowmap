<?php

namespace App\Entity;

use App\Repository\MarkerTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MarkerTypeRepository::class)
 */
class MarkerType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $icon;

    /**
     * @ORM\ManyToOne(targetEntity=MarkerType::class, inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=MarkerType::class, mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\Column(type="boolean")
     */
    private $displayInControls;

    /**
     * @ORM\OneToMany(targetEntity=Marker::class, mappedBy="type")
     */
    private $markers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $icon_width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $icon_height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $icon_anchor;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disabled;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $background;

    /**
     * @ORM\Column(type="boolean")
     */
    private $defaultShow;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->markers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getDisplayInControls(): ?bool
    {
        return $this->displayInControls;
    }

    public function setDisplayInControls(bool $displayInControls): self
    {
        $this->displayInControls = $displayInControls;

        return $this;
    }

    /**
     * @return Collection|Marker[]
     */
    public function getMarkers(): Collection
    {
        return $this->markers;
    }

    public function addMarker(Marker $marker): self
    {
        if (!$this->markers->contains($marker)) {
            $this->markers[] = $marker;
            $marker->setType($this);
        }

        return $this;
    }

    public function removeMarker(Marker $marker): self
    {
        if ($this->markers->removeElement($marker)) {
            // set the owning side to null (unless already changed)
            if ($marker->getType() === $this) {
                $marker->setType(null);
            }
        }

        return $this;
    }

    public function getIconWidth(): ?int
    {
        return $this->icon_width;
    }

    public function setIconWidth(?int $icon_width): self
    {
        $this->icon_width = $icon_width;

        return $this;
    }

    public function getIconHeight(): ?int
    {
        return $this->icon_height;
    }

    public function setIconHeight(?int $icon_height): self
    {
        $this->icon_height = $icon_height;

        return $this;
    }

    public function getIconAnchor(): ?int
    {
        return $this->icon_anchor;
    }

    public function setIconAnchor(?int $icon_anchor): self
    {
        $this->icon_anchor = $icon_anchor;

        return $this;
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function getBackground(): ?int
    {
        return $this->background;
    }

    public function setBackground(?int $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getDefaultShow(): ?bool
    {
        return $this->defaultShow;
    }

    public function setDefaultShow(bool $defaultShow): self
    {
        $this->defaultShow = $defaultShow;

        return $this;
    }
}
