<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin.html.twig */
class __TwigTemplate_a2143e5dc9e22e15431bf99e1c61f6c28886d58e00f3553e1a85bda486528780 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "admin.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "<div id=\"admin\">
\t";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "success"], "method", false, false, false, 6));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 7
            echo "\t\t<div class=\"alert alert-success\">
\t\t\t";
            // line 8
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
\t\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "flashes", [0 => "error"], "method", false, false, false, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 12
            echo "\t\t<div class=\"alert alert-warning\">
\t\t\t";
            // line 13
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
\t\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "\t<div id=\"map\"></div>
\t<h1>Create new Marker</h1>
\t<hr>
\t<form method=\"post\" action=\"/admin/new\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"type\">Choose Marker Type</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<select name=\"type\" id=\"markertypeselect\">
\t\t\t\t\t";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["markerTypes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mt"]) {
            // line 27
            echo "\t\t\t\t\t\t";
            if ((-1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 27)), 1))) {
                // line 28
                echo "\t\t\t\t \t\t\t<option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "id", [], "any", false, false, false, 28), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 28), "html", null, true);
                echo "</option>
\t\t\t\t\t\t";
            }
            // line 30
            echo "\t\t\t\t \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"x\">x coordinate</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"x\" style=\"width:100px; margin-bottom: 5px\"></input>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"z\">z coordinate</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"z\" style=\"width:100px\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"title\">Title</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"title\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"description\">Description</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<textarea name=\"description\" rows=\"4\" cols=\"50\"></textarea>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"image\">Image</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"image\"></input>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"image_author\">Image Author</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"image_author\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"icon\">Icon</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"icon\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["markerTypes"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "icon", [], "any", false, false, false, 92), "html", null, true);
        echo "\" id=\"markertypeicon\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"icon_width\">Icon Width</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"icon_width\" placeholder=\"";
        // line 101
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["markerTypes"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[0] ?? null) : null), "iconwidth", [], "any", false, false, false, 101), "html", null, true);
        echo "\" style=\"width: 80px\" id=\"markertypeiconwidth\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"icon_height\">Icon Height</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"icon_height\" placeholder=\"";
        // line 110
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["markerTypes"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[0] ?? null) : null), "iconheight", [], "any", false, false, false, 110), "html", null, true);
        echo "\" style=\"width: 80px\" id=\"markertypeiconheight\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"icon_anchor\">Icon Anchor</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"text\" name=\"icon_anchor\" placeholder=\"";
        // line 119
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["markerTypes"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[0] ?? null) : null), "iconanchor", [], "any", false, false, false, 119), "html", null, true);
        echo "\" style=\"width: 80px\" id=\"markertypeiconanchor\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-3\">
\t\t\t\t<label for=\"icon_anchor\">Underground</label>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<input type=\"checkbox\" name=\"underground\"></input>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<button type=\"submit\" class=\"btn btn-success\">Go</button>
\t</form>
</div>
<div id=\"markerTypes\">
\t";
        // line 136
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["markerTypes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mt"]) {
            // line 137
            echo "\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 137)) {
                // line 138
                echo "\t\t\t<div
\t\t\tdata-id=\"";
                // line 139
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "id", [], "any", false, false, false, 139), "html", null, true);
                echo "\"
\t\t\tdata-title=\"";
                // line 140
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 140), "html", null, true);
                echo "\"
\t\t\tdata-parent=\"";
                // line 141
                if (twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 141)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 141), "id", [], "any", false, false, false, 141), "html", null, true);
                }
                echo "\"
\t\t\tdata-icon=\"";
                // line 142
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "icon", [], "any", false, false, false, 142), "html", null, true);
                echo "\"
\t\t\tdata-icon-width=\"";
                // line 143
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "iconwidth", [], "any", false, false, false, 143), "html", null, true);
                echo "\"
\t\t\tdata-icon-height=\"";
                // line 144
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "iconheight", [], "any", false, false, false, 144), "html", null, true);
                echo "\"
\t\t\tdata-icon-anchor=\"";
                // line 145
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "iconanchor", [], "any", false, false, false, 145), "html", null, true);
                echo "\"
\t\t\tdata-disabled=\"";
                // line 146
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 146), "html", null, true);
                echo "\"
\t\t\t></div>
\t\t";
            }
            // line 149
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 150
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 150,  284 => 149,  278 => 146,  274 => 145,  270 => 144,  266 => 143,  262 => 142,  256 => 141,  252 => 140,  248 => 139,  245 => 138,  242 => 137,  238 => 136,  218 => 119,  206 => 110,  194 => 101,  182 => 92,  119 => 31,  113 => 30,  105 => 28,  102 => 27,  98 => 26,  86 => 16,  77 => 13,  74 => 12,  69 => 11,  60 => 8,  57 => 7,  53 => 6,  50 => 5,  46 => 4,  35 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "admin.html.twig", "/var/www/meadowmap/templates/admin.html.twig");
    }
}
