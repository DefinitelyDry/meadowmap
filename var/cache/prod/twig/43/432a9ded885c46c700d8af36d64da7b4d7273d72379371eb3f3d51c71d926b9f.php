<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_30d2147208fafbfeea02e4f15050afdb4ccf6b6da35891378830b143ae0d453c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "<div id=\"map\"></div>
<div id=\"menu\">
\t<div class=\"menu-top row\">
\t\t<div class=\"col-md-6\">
\t\t\t<h1>Interactive Meadow Map <small>by Nemo and c42</small></h1>
\t\t</div>
\t\t<div class=\"col-md-6\">
\t\t\t<div class=\"togglemenu\">
\t\t\t\t<span>Click here to filter layers and for more information</span> <i class=\"fas fa-chevron-up\"></i>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"menu-content row\">
\t\t<div class=\"col-md-5 menu-info\">
\t\t\tThis purely fan-made project aims at providing Meadow users with a precise map with as many details as possible. The borders of the map and all markers are precisely positioned, using game coordinates as a reference, the maps North however corresponds to in game East. All names used on the map are not official. The use of the textures has been approved by the game developers. The map will be kept updated as new content is released. Check out our <a href=\"https://steamcommunity.com/sharedfiles/filedetails/?id=2276192929\">page on Steam</a> for future plans. The work poured into this project is and will continue to be a hobby, if you however wish to show support, I have set up a <a href=\"#\">Flattr page</a> for donations to cover server costs only. We hope our map helps you navigate through the world of Meadow - see you in game!
\t\t\t<div class=\"menu-links\">
\t\t\t\t<span>© Copyright 2020, last updated November 3rd 2020</span>
\t\t\t\t<a href=\"/imprint\">Imprint</a>
\t\t\t\t<a href=\"/privacy\">Privacy</a>
\t\t\t\t<a href=\"#\">Flattr</a>
\t\t\t\t<a href=\"https://steamcommunity.com/sharedfiles/filedetails/?id=2276192929\">Plans</a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-7 menu-filters row\">
\t\t\t<div class=\"single-filters\">
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_inclines.png\" />
\t\t\t\t\t\t<label>Inclines <input type=\"checkbox\" checked data-layer=\"Inclines\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_rises.png\" />
\t\t\t\t\t\t<label>Rises <input type=\"checkbox\" checked data-layer=\"Rises\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_foliage.png\" />
\t\t\t\t\t\t<label>Catnip and Puffballs <input type=\"checkbox\" checked data-layer=\"Catnip and Puffballs\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["markerTypes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mt"]) {
            // line 50
            echo "\t\t\t\t";
            if ( !twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 50)) {
                // line 51
                echo "\t\t\t\t\t<div class=\"filter";
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 51)), 0))) {
                    echo " col filter-parent row";
                }
                echo "\" data-id=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "id", [], "any", false, false, false, 51), "html", null, true);
                echo "\" ";
                if ( !twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 51)) {
                    echo "style=\"display:none\"";
                }
                echo ">
\t\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t\t<img class=\"filter-icon\" src=\"";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "icon", [], "any", false, false, false, 53), "html", null, true);
                echo "\" />
\t\t\t\t\t\t\t<label>";
                // line 54
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 54), "html", null, true);
                echo " <input type=\"checkbox\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["mt"], "defaultshow", [], "any", false, false, false, 54) &&  !twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 54))) {
                    echo "checked";
                }
                echo " data-layer=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 54), "html", null, true);
                echo "\"></label> <i class=\"filter-check fas fa-check\" ";
                if (( !twig_get_attribute($this->env, $this->source, $context["mt"], "defaultshow", [], "any", false, false, false, 54) || twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 54))) {
                    echo "style=\"opacity: 0.3\"";
                }
                echo "></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                // line 56
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 56)), 0))) {
                    // line 57
                    echo "\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 57));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 58
                        echo "\t\t\t\t\t\t\t\t";
                        if ((0 === twig_compare((twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 58) % 6), 0))) {
                            // line 59
                            echo "\t\t\t\t\t\t\t\t\t<div class=\"col filter-children-group\">
\t\t\t\t\t\t\t\t";
                        }
                        // line 61
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"filter filter-child\" data-id=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "id", [], "any", false, false, false, 61), "html", null, true);
                        echo "\" ";
                        if ( !twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 61)) {
                            echo "style=\"display:none\"";
                        }
                        echo ">
\t\t\t\t\t\t\t\t\t\t<img class=\"filter-icon\" src=\"";
                        // line 62
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "icon", [], "any", false, false, false, 62), "html", null, true);
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t<label ";
                        // line 63
                        if (twig_get_attribute($this->env, $this->source, $context["child"], "disabled", [], "any", false, false, false, 63)) {
                            echo "style=\"color: #ffc5c5\"";
                        }
                        echo ">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 63), "html", null, true);
                        echo " <input type=\"checkbox\" ";
                        if ((twig_get_attribute($this->env, $this->source, $context["child"], "defaultshow", [], "any", false, false, false, 63) &&  !twig_get_attribute($this->env, $this->source, $context["child"], "disabled", [], "any", false, false, false, 63))) {
                            echo "checked";
                        }
                        echo " data-layer=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 63), "html", null, true);
                        echo "\"></label> <i class=\"filter-check fas fa-check\" ";
                        if (( !twig_get_attribute($this->env, $this->source, $context["child"], "defaultshow", [], "any", false, false, false, 63) || twig_get_attribute($this->env, $this->source, $context["child"], "disabled", [], "any", false, false, false, 63))) {
                            echo "style=\"opacity: 0.3\"";
                        }
                        echo "></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                        // line 65
                        if ((0 === twig_compare((twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 65) % 6), 5))) {
                            // line 66
                            echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                        }
                        // line 68
                        echo "\t\t\t\t\t\t\t";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 69
                    echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
                }
                // line 73
                echo "\t\t\t\t\t</div>
\t\t\t\t";
            }
            // line 75
            echo "\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "\t\t</div>
\t</div>
</div>
<div id=\"markerTypes\">
\t";
        // line 80
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["markerTypes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mt"]) {
            // line 81
            echo "\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 81)) {
                // line 82
                echo "\t\t\t<div
\t\t\tdata-id=\"";
                // line 83
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "id", [], "any", false, false, false, 83), "html", null, true);
                echo "\"
\t\t\tdata-title=\"";
                // line 84
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 84), "html", null, true);
                echo "\"
\t\t\tdata-parent=\"";
                // line 85
                if (twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 85)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 85), "id", [], "any", false, false, false, 85), "html", null, true);
                }
                echo "\"
\t\t\tdata-icon=\"";
                // line 86
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "icon", [], "any", false, false, false, 86), "html", null, true);
                echo "\"
\t\t\tdata-defaultshow=\"";
                // line 87
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "defaultshow", [], "any", false, false, false, 87), "html", null, true);
                echo "\"
\t\t\tdata-disabled=\"";
                // line 88
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 88), "html", null, true);
                echo "\"
\t\t\t></div>
\t\t";
            }
            // line 91
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "</div>
<div id=\"markers\">
\t";
        // line 94
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["markers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 95
            echo "\t\t";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 95), "displayInControls", [], "any", false, false, false, 95)) {
                // line 96
                echo "\t\t\t<div
\t\t\tdata-type=\"";
                // line 97
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 97), "title", [], "any", false, false, false, 97), "html", null, true);
                echo "\"
\t\t\tdata-x=\"";
                // line 98
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "x", [], "any", false, false, false, 98), "html", null, true);
                echo "\"
\t\t\tdata-z=\"";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "z", [], "any", false, false, false, 99), "html", null, true);
                echo "\"
\t\t\tdata-title=\"";
                // line 100
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "title", [], "any", false, false, false, 100), "html", null, true);
                echo "\"
\t\t\tdata-description=\"";
                // line 101
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "description", [], "any", false, false, false, 101), "html", null, true);
                echo "\"
\t\t\tdata-image=\"";
                // line 102
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "image", [], "any", false, false, false, 102), "html", null, true);
                echo "\"
\t\t\tdata-image-author=\"";
                // line 103
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "imageAuthor", [], "any", false, false, false, 103), "html", null, true);
                echo "\"
\t\t\tdata-icon=\"";
                // line 104
                if (twig_get_attribute($this->env, $this->source, $context["m"], "icon", [], "any", false, false, false, 104)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "icon", [], "any", false, false, false, 104), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 104), "icon", [], "any", false, false, false, 104), "html", null, true);
                }
                echo "\"
\t\t\tdata-icon-width=\"";
                // line 105
                if (twig_get_attribute($this->env, $this->source, $context["m"], "iconWidth", [], "any", false, false, false, 105)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "iconWidth", [], "any", false, false, false, 105), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "parent", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "parent", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105), "html", null, true);
                } else {
                    echo "50";
                }
                echo "\"
\t\t\tdata-icon-height=\"";
                // line 106
                if (twig_get_attribute($this->env, $this->source, $context["m"], "iconHeight", [], "any", false, false, false, 106)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "iconHeight", [], "any", false, false, false, 106), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "parent", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "parent", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106), "html", null, true);
                } else {
                    echo "50";
                }
                echo "\"
\t\t\tdata-icon-anchor=\"";
                // line 107
                if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["m"], "iconAnchor", [], "any", false, false, false, 107), null))) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "iconAnchor", [], "any", false, false, false, 107), "html", null, true);
                } elseif ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), null))) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), "html", null, true);
                } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "parent", [], "any", false, false, false, 107) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "parent", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), null)))) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "parent", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), "html", null, true);
                } else {
                    echo "25";
                }
                echo "\"
\t\t\tdata-disabled=\"";
                // line 108
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 108), "disabled", [], "any", false, false, false, 108), "html", null, true);
                echo "\"
\t\t\tdata-underground=\"";
                // line 109
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "underground", [], "any", false, false, false, 109), "html", null, true);
                echo "\"
\t\t\tdata-background=\"";
                // line 110
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 110), "background", [], "any", false, false, false, 110)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 110), "background", [], "any", false, false, false, 110), "html", null, true);
                } else {
                    echo "0";
                }
                echo "\"
\t\t\t></div>
\t\t";
            }
            // line 113
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  391 => 114,  385 => 113,  375 => 110,  371 => 109,  367 => 108,  355 => 107,  343 => 106,  331 => 105,  323 => 104,  319 => 103,  315 => 102,  311 => 101,  307 => 100,  303 => 99,  299 => 98,  295 => 97,  292 => 96,  289 => 95,  285 => 94,  281 => 92,  275 => 91,  269 => 88,  265 => 87,  261 => 86,  255 => 85,  251 => 84,  247 => 83,  244 => 82,  241 => 81,  237 => 80,  231 => 76,  225 => 75,  221 => 73,  215 => 69,  201 => 68,  197 => 66,  195 => 65,  176 => 63,  172 => 62,  163 => 61,  159 => 59,  156 => 58,  138 => 57,  136 => 56,  121 => 54,  117 => 53,  103 => 51,  100 => 50,  96 => 49,  50 => 5,  46 => 4,  35 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "index.html.twig", "/var/www/meadowmap/templates/index.html.twig");
    }
}
