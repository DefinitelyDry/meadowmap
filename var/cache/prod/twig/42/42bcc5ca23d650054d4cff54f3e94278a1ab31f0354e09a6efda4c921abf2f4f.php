<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_68a200374b91c44702cada1c9b4a5a9a19b288de531ae8da472bb1555240429c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"flattr:id\" content=\"vjdoyj\"> 
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"shortcut icon\" type=\"image/png\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon.png"), "html", null, true);
        echo "\"/>
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "    </head>
    <body>
        ";
        // line 22
        $this->displayBlock('body', $context, $blocks);
        // line 23
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 77
        echo "    </body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Interactive Meadow Map";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "            <!-- Bootstrap, FontAwesome, Nunito Sans -->
        \t<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("twbs/bootstrap/dist/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        \t<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("fa/css/all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap\" rel=\"stylesheet\">
             <link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.7.1/dist/leaflet.css\"
\t\t\t   integrity=\"sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==\"
\t\t\t   crossorigin=\"\"/>

            <!-- Custom CSS -->
            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        ";
    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 23
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "            <!-- JQuery, Leaflet -->
        \t<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.js"), "html", null, true);
        echo "\"></script>
            <script src=\"https://unpkg.com/leaflet@1.7.1/dist/leaflet.js\"
\t\t\t   integrity=\"sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==\"
\t\t\t   crossorigin=\"\"></script>
\t\t\t<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

            <!-- Custom JS -->
            <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
            
            <!-- Cookie Notice -->
            <script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/cookie.notice.js"), "html", null, true);
        echo "\"></script>
            <!-- <script>
                new cookieNoticeJS({
                   // Localizations of the notice message
                   'messageLocales': {
                     'en': 'Um Ihnen ein optimales Erlebnis auf unserer Website zu bieten, nutzen wir Cookies. Lesen Sie mehr über unsere Verwendungszwecke in unserer'
                   },
                  
                   // Localizations of the dismiss button text
                   'buttonLocales': {
                     'en': 'Akzeptieren'
                   },
                   
                   // Shows the \"learn more button (default=false)
                   'learnMoreLinkEnabled':true,
                   
                   // The href of the learn more link must be applied if (learnMoreLinkEnabled=true) 
                   'learnMoreLinkHref':'/privacy',
                   
                   // Text for optional learn more button
                   'learnMoreLinkText':{
                       'en':'Datenschutzerklärung.'
                   },
                   
                   // Dismiss button background color
                   'buttonBgColor': '#fff',  
                   
                   // Dismiss button text color
                   'buttonTextColor': '#196bba', 
                     
                   // Notice background color
                   'noticeBgColor': '#196bba', 
                      
                   // Notice text color
                   'noticeTextColor': '#fff', 
                   
                   // the lernMoreLink color (default='#009fdd') 
                   'linkColor':'#dddddd'
                      
                }); -->
</script>
        ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 35,  133 => 32,  127 => 29,  120 => 25,  117 => 24,  113 => 23,  107 => 22,  101 => 18,  91 => 11,  87 => 10,  84 => 9,  80 => 8,  73 => 6,  67 => 77,  64 => 23,  62 => 22,  58 => 20,  56 => 8,  52 => 7,  48 => 6,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/var/www/meadowmap/templates/base.html.twig");
    }
}
