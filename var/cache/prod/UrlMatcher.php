<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\AdminController::index'], null, null, null, false, false, null]],
        '/admin/new' => [[['_route' => 'new', '_controller' => 'App\\Controller\\AdminController::new'], null, null, null, false, false, null]],
        '/privacy' => [[['_route' => 'privacy', '_controller' => 'App\\Controller\\IndexController::privacy'], null, null, null, false, false, null]],
        '/imprint' => [[['_route' => 'imprint', '_controller' => 'App\\Controller\\IndexController::imprint'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
    ],
    [ // $dynamicRoutes
    ],
    null, // $checkCondition
];
