<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_f3eac3be92c04edcb6e93b0273d239568684a5f73dbdabb86c2299363aeac079 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"flattr:id\" content=\"vjdoyj\"> 
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"shortcut icon\" type=\"image/png\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon.png"), "html", null, true);
        echo "\"/>
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "    </head>
    <body>
        ";
        // line 22
        $this->displayBlock('body', $context, $blocks);
        // line 23
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 77
        echo "    </body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Interactive Meadow Map";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "            <!-- Bootstrap, FontAwesome, Nunito Sans -->
        \t<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("twbs/bootstrap/dist/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        \t<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("fa/css/all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <link href=\"https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap\" rel=\"stylesheet\">
             <link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.7.1/dist/leaflet.css\"
\t\t\t   integrity=\"sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==\"
\t\t\t   crossorigin=\"\"/>

            <!-- Custom CSS -->
            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 22
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 23
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 24
        echo "            <!-- JQuery, Leaflet -->
        \t<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.js"), "html", null, true);
        echo "\"></script>
            <script src=\"https://unpkg.com/leaflet@1.7.1/dist/leaflet.js\"
\t\t\t   integrity=\"sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==\"
\t\t\t   crossorigin=\"\"></script>
\t\t\t<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>

            <!-- Custom JS -->
            <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
            
            <!-- Cookie Notice -->
            <script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/cookie.notice.js"), "html", null, true);
        echo "\"></script>
            <!-- <script>
                new cookieNoticeJS({
                   // Localizations of the notice message
                   'messageLocales': {
                     'en': 'Um Ihnen ein optimales Erlebnis auf unserer Website zu bieten, nutzen wir Cookies. Lesen Sie mehr über unsere Verwendungszwecke in unserer'
                   },
                  
                   // Localizations of the dismiss button text
                   'buttonLocales': {
                     'en': 'Akzeptieren'
                   },
                   
                   // Shows the \"learn more button (default=false)
                   'learnMoreLinkEnabled':true,
                   
                   // The href of the learn more link must be applied if (learnMoreLinkEnabled=true) 
                   'learnMoreLinkHref':'/privacy',
                   
                   // Text for optional learn more button
                   'learnMoreLinkText':{
                       'en':'Datenschutzerklärung.'
                   },
                   
                   // Dismiss button background color
                   'buttonBgColor': '#fff',  
                   
                   // Dismiss button text color
                   'buttonTextColor': '#196bba', 
                     
                   // Notice background color
                   'noticeBgColor': '#196bba', 
                      
                   // Notice text color
                   'noticeTextColor': '#fff', 
                   
                   // the lernMoreLink color (default='#009fdd') 
                   'linkColor':'#dddddd'
                      
                }); -->
</script>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 35,  160 => 32,  154 => 29,  147 => 25,  144 => 24,  137 => 23,  125 => 22,  116 => 18,  106 => 11,  102 => 10,  99 => 9,  92 => 8,  79 => 6,  70 => 77,  67 => 23,  65 => 22,  61 => 20,  59 => 8,  55 => 7,  51 => 6,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"flattr:id\" content=\"vjdoyj\"> 
        <title>{% block title %}Interactive Meadow Map{% endblock %}</title>
        <link rel=\"shortcut icon\" type=\"image/png\" href=\"{{ asset('img/favicon.png') }}\"/>
        {% block stylesheets %}
            <!-- Bootstrap, FontAwesome, Nunito Sans -->
        \t<link href=\"{{ asset('twbs/bootstrap/dist/css/bootstrap.css') }}\" rel=\"stylesheet\">
        \t<link href=\"{{ asset('fa/css/all.css') }}\" rel=\"stylesheet\">
            <link href=\"https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap\" rel=\"stylesheet\">
             <link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.7.1/dist/leaflet.css\"
\t\t\t   integrity=\"sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==\"
\t\t\t   crossorigin=\"\"/>

            <!-- Custom CSS -->
            <link href=\"{{ asset('css/main.css') }}\" rel=\"stylesheet\">
        {% endblock %}
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}
            <!-- JQuery, Leaflet -->
        \t<script src=\"{{ asset('js/jquery.js') }}\"></script>
            <script src=\"https://unpkg.com/leaflet@1.7.1/dist/leaflet.js\"
\t\t\t   integrity=\"sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==\"
\t\t\t   crossorigin=\"\"></script>
\t\t\t<script src=\"{{ asset('js/jquery-ui.min.js') }}\"></script>

            <!-- Custom JS -->
            <script src=\"{{ asset('js/main.js') }}\"></script>
            
            <!-- Cookie Notice -->
            <script src=\"{{ asset('js/cookie.notice.js') }}\"></script>
            <!-- <script>
                new cookieNoticeJS({
                   // Localizations of the notice message
                   'messageLocales': {
                     'en': 'Um Ihnen ein optimales Erlebnis auf unserer Website zu bieten, nutzen wir Cookies. Lesen Sie mehr über unsere Verwendungszwecke in unserer'
                   },
                  
                   // Localizations of the dismiss button text
                   'buttonLocales': {
                     'en': 'Akzeptieren'
                   },
                   
                   // Shows the \"learn more button (default=false)
                   'learnMoreLinkEnabled':true,
                   
                   // The href of the learn more link must be applied if (learnMoreLinkEnabled=true) 
                   'learnMoreLinkHref':'/privacy',
                   
                   // Text for optional learn more button
                   'learnMoreLinkText':{
                       'en':'Datenschutzerklärung.'
                   },
                   
                   // Dismiss button background color
                   'buttonBgColor': '#fff',  
                   
                   // Dismiss button text color
                   'buttonTextColor': '#196bba', 
                     
                   // Notice background color
                   'noticeBgColor': '#196bba', 
                      
                   // Notice text color
                   'noticeTextColor': '#fff', 
                   
                   // the lernMoreLink color (default='#009fdd') 
                   'linkColor':'#dddddd'
                      
                }); -->
</script>
        {% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/meadowmap/templates/base.html.twig");
    }
}
