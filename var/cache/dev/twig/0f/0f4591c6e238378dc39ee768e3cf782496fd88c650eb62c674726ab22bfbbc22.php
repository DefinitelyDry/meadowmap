<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_af5822a840f39c13b52a69f3d8091e562c038392d1c7bae80a1c7c0b7f8f602d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<div id=\"map\"></div>
<div id=\"menu\">
\t<div class=\"menu-top row\">
\t\t<div class=\"col-md-6\">
\t\t\t<h1>Interactive Meadow Map <small>by Nemo and c42</small></h1>
\t\t</div>
\t\t<div class=\"col-md-6\">
\t\t\t<div class=\"togglemenu\">
\t\t\t\t<span>Click here to filter layers and for more information</span> <i class=\"fas fa-chevron-up\"></i>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"menu-content row\">
\t\t<div class=\"col-md-5 menu-info\">
\t\t\tThis purely fan-made project aims at providing Meadow users with a precise map with as many details as possible. The borders of the map and all markers are precisely positioned, using game coordinates as a reference, the maps North however corresponds to in game East. All names used on the map are not official. The use of the textures has been approved by the game developers. The map will be kept updated as new content is released. Check out our <a href=\"https://steamcommunity.com/sharedfiles/filedetails/?id=2276192929\">page on Steam</a> for future plans. The work poured into this project is and will continue to be a hobby, if you however wish to show support, I have set up a <a href=\"#\">Flattr page</a> for donations to cover server costs only. We hope our map helps you navigate through the world of Meadow - see you in game!
\t\t\t<div class=\"menu-links\">
\t\t\t\t<span>© Copyright 2020, last updated November 3rd 2020</span>
\t\t\t\t<a href=\"/imprint\">Imprint</a>
\t\t\t\t<a href=\"/privacy\">Privacy</a>
\t\t\t\t<a href=\"#\">Flattr</a>
\t\t\t\t<a href=\"https://steamcommunity.com/sharedfiles/filedetails/?id=2276192929\">Plans</a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-7 menu-filters row\">
\t\t\t<div class=\"single-filters\">
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_inclines.png\" />
\t\t\t\t\t\t<label>Inclines <input type=\"checkbox\" checked data-layer=\"Inclines\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_rises.png\" />
\t\t\t\t\t\t<label>Rises <input type=\"checkbox\" checked data-layer=\"Rises\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_foliage.png\" />
\t\t\t\t\t\t<label>Catnip and Puffballs <input type=\"checkbox\" checked data-layer=\"Catnip and Puffballs\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["markerTypes"]) || array_key_exists("markerTypes", $context) ? $context["markerTypes"] : (function () { throw new RuntimeError('Variable "markerTypes" does not exist.', 49, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["mt"]) {
            // line 50
            echo "\t\t\t\t";
            if ( !twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 50)) {
                // line 51
                echo "\t\t\t\t\t<div class=\"filter";
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 51)), 0))) {
                    echo " col filter-parent row";
                }
                echo "\" data-id=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "id", [], "any", false, false, false, 51), "html", null, true);
                echo "\" ";
                if ( !twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 51)) {
                    echo "style=\"display:none\"";
                }
                echo ">
\t\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t\t<img class=\"filter-icon\" src=\"";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "icon", [], "any", false, false, false, 53), "html", null, true);
                echo "\" />
\t\t\t\t\t\t\t<label>";
                // line 54
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 54), "html", null, true);
                echo " <input type=\"checkbox\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["mt"], "defaultshow", [], "any", false, false, false, 54) &&  !twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 54))) {
                    echo "checked";
                }
                echo " data-layer=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 54), "html", null, true);
                echo "\"></label> <i class=\"filter-check fas fa-check\" ";
                if (( !twig_get_attribute($this->env, $this->source, $context["mt"], "defaultshow", [], "any", false, false, false, 54) || twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 54))) {
                    echo "style=\"opacity: 0.3\"";
                }
                echo "></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                // line 56
                if ((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 56)), 0))) {
                    // line 57
                    echo "\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["mt"], "children", [], "any", false, false, false, 57));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 58
                        echo "\t\t\t\t\t\t\t\t";
                        if ((0 === twig_compare((twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 58) % 6), 0))) {
                            // line 59
                            echo "\t\t\t\t\t\t\t\t\t<div class=\"col filter-children-group\">
\t\t\t\t\t\t\t\t";
                        }
                        // line 61
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"filter filter-child\" data-id=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "id", [], "any", false, false, false, 61), "html", null, true);
                        echo "\" ";
                        if ( !twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 61)) {
                            echo "style=\"display:none\"";
                        }
                        echo ">
\t\t\t\t\t\t\t\t\t\t<img class=\"filter-icon\" src=\"";
                        // line 62
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "icon", [], "any", false, false, false, 62), "html", null, true);
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t<label ";
                        // line 63
                        if (twig_get_attribute($this->env, $this->source, $context["child"], "disabled", [], "any", false, false, false, 63)) {
                            echo "style=\"color: #ffc5c5\"";
                        }
                        echo ">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 63), "html", null, true);
                        echo " <input type=\"checkbox\" ";
                        if ((twig_get_attribute($this->env, $this->source, $context["child"], "defaultshow", [], "any", false, false, false, 63) &&  !twig_get_attribute($this->env, $this->source, $context["child"], "disabled", [], "any", false, false, false, 63))) {
                            echo "checked";
                        }
                        echo " data-layer=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "title", [], "any", false, false, false, 63), "html", null, true);
                        echo "\"></label> <i class=\"filter-check fas fa-check\" ";
                        if (( !twig_get_attribute($this->env, $this->source, $context["child"], "defaultshow", [], "any", false, false, false, 63) || twig_get_attribute($this->env, $this->source, $context["child"], "disabled", [], "any", false, false, false, 63))) {
                            echo "style=\"opacity: 0.3\"";
                        }
                        echo "></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                        // line 65
                        if ((0 === twig_compare((twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 65) % 6), 5))) {
                            // line 66
                            echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                        }
                        // line 68
                        echo "\t\t\t\t\t\t\t";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 69
                    echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t";
                }
                // line 73
                echo "\t\t\t\t\t</div>
\t\t\t\t";
            }
            // line 75
            echo "\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "\t\t</div>
\t</div>
</div>
<div id=\"markerTypes\">
\t";
        // line 80
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["markerTypes"]) || array_key_exists("markerTypes", $context) ? $context["markerTypes"] : (function () { throw new RuntimeError('Variable "markerTypes" does not exist.', 80, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["mt"]) {
            // line 81
            echo "\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["mt"], "displayInControls", [], "any", false, false, false, 81)) {
                // line 82
                echo "\t\t\t<div
\t\t\tdata-id=\"";
                // line 83
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "id", [], "any", false, false, false, 83), "html", null, true);
                echo "\"
\t\t\tdata-title=\"";
                // line 84
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "title", [], "any", false, false, false, 84), "html", null, true);
                echo "\"
\t\t\tdata-parent=\"";
                // line 85
                if (twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 85)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mt"], "parent", [], "any", false, false, false, 85), "id", [], "any", false, false, false, 85), "html", null, true);
                }
                echo "\"
\t\t\tdata-icon=\"";
                // line 86
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "icon", [], "any", false, false, false, 86), "html", null, true);
                echo "\"
\t\t\tdata-defaultshow=\"";
                // line 87
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "defaultshow", [], "any", false, false, false, 87), "html", null, true);
                echo "\"
\t\t\tdata-disabled=\"";
                // line 88
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mt"], "disabled", [], "any", false, false, false, 88), "html", null, true);
                echo "\"
\t\t\t></div>
\t\t";
            }
            // line 91
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "</div>
<div id=\"markers\">
\t";
        // line 94
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["markers"]) || array_key_exists("markers", $context) ? $context["markers"] : (function () { throw new RuntimeError('Variable "markers" does not exist.', 94, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 95
            echo "\t\t";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 95), "displayInControls", [], "any", false, false, false, 95)) {
                // line 96
                echo "\t\t\t<div
\t\t\tdata-type=\"";
                // line 97
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 97), "title", [], "any", false, false, false, 97), "html", null, true);
                echo "\"
\t\t\tdata-x=\"";
                // line 98
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "x", [], "any", false, false, false, 98), "html", null, true);
                echo "\"
\t\t\tdata-z=\"";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "z", [], "any", false, false, false, 99), "html", null, true);
                echo "\"
\t\t\tdata-title=\"";
                // line 100
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "title", [], "any", false, false, false, 100), "html", null, true);
                echo "\"
\t\t\tdata-description=\"";
                // line 101
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "description", [], "any", false, false, false, 101), "html", null, true);
                echo "\"
\t\t\tdata-image=\"";
                // line 102
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "image", [], "any", false, false, false, 102), "html", null, true);
                echo "\"
\t\t\tdata-image-author=\"";
                // line 103
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "imageAuthor", [], "any", false, false, false, 103), "html", null, true);
                echo "\"
\t\t\tdata-icon=\"";
                // line 104
                if (twig_get_attribute($this->env, $this->source, $context["m"], "icon", [], "any", false, false, false, 104)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "icon", [], "any", false, false, false, 104), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 104), "icon", [], "any", false, false, false, 104), "html", null, true);
                }
                echo "\"
\t\t\tdata-icon-width=\"";
                // line 105
                if (twig_get_attribute($this->env, $this->source, $context["m"], "iconWidth", [], "any", false, false, false, 105)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "iconWidth", [], "any", false, false, false, 105), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "parent", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 105), "parent", [], "any", false, false, false, 105), "iconWidth", [], "any", false, false, false, 105), "html", null, true);
                } else {
                    echo "50";
                }
                echo "\"
\t\t\tdata-icon-height=\"";
                // line 106
                if (twig_get_attribute($this->env, $this->source, $context["m"], "iconHeight", [], "any", false, false, false, 106)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "iconHeight", [], "any", false, false, false, 106), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106), "html", null, true);
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "parent", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 106), "parent", [], "any", false, false, false, 106), "iconHeight", [], "any", false, false, false, 106), "html", null, true);
                } else {
                    echo "50";
                }
                echo "\"
\t\t\tdata-icon-anchor=\"";
                // line 107
                if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, $context["m"], "iconAnchor", [], "any", false, false, false, 107), null))) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "iconAnchor", [], "any", false, false, false, 107), "html", null, true);
                } elseif ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), null))) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), "html", null, true);
                } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "parent", [], "any", false, false, false, 107) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "parent", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), null)))) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 107), "parent", [], "any", false, false, false, 107), "iconAnchor", [], "any", false, false, false, 107), "html", null, true);
                } else {
                    echo "25";
                }
                echo "\"
\t\t\tdata-disabled=\"";
                // line 108
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 108), "disabled", [], "any", false, false, false, 108), "html", null, true);
                echo "\"
\t\t\tdata-underground=\"";
                // line 109
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "underground", [], "any", false, false, false, 109), "html", null, true);
                echo "\"
\t\t\tdata-background=\"";
                // line 110
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 110), "background", [], "any", false, false, false, 110)) {
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "type", [], "any", false, false, false, 110), "background", [], "any", false, false, false, 110), "html", null, true);
                } else {
                    echo "0";
                }
                echo "\"
\t\t\t></div>
\t\t";
            }
            // line 113
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  400 => 114,  394 => 113,  384 => 110,  380 => 109,  376 => 108,  364 => 107,  352 => 106,  340 => 105,  332 => 104,  328 => 103,  324 => 102,  320 => 101,  316 => 100,  312 => 99,  308 => 98,  304 => 97,  301 => 96,  298 => 95,  294 => 94,  290 => 92,  284 => 91,  278 => 88,  274 => 87,  270 => 86,  264 => 85,  260 => 84,  256 => 83,  253 => 82,  250 => 81,  246 => 80,  240 => 76,  234 => 75,  230 => 73,  224 => 69,  210 => 68,  206 => 66,  204 => 65,  185 => 63,  181 => 62,  172 => 61,  168 => 59,  165 => 58,  147 => 57,  145 => 56,  130 => 54,  126 => 53,  112 => 51,  109 => 50,  105 => 49,  59 => 5,  52 => 4,  35 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# templates/index.html.twig #}
{% extends \"base.html.twig\" %}

{% block body %}
<div id=\"map\"></div>
<div id=\"menu\">
\t<div class=\"menu-top row\">
\t\t<div class=\"col-md-6\">
\t\t\t<h1>Interactive Meadow Map <small>by Nemo and c42</small></h1>
\t\t</div>
\t\t<div class=\"col-md-6\">
\t\t\t<div class=\"togglemenu\">
\t\t\t\t<span>Click here to filter layers and for more information</span> <i class=\"fas fa-chevron-up\"></i>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"menu-content row\">
\t\t<div class=\"col-md-5 menu-info\">
\t\t\tThis purely fan-made project aims at providing Meadow users with a precise map with as many details as possible. The borders of the map and all markers are precisely positioned, using game coordinates as a reference, the maps North however corresponds to in game East. All names used on the map are not official. The use of the textures has been approved by the game developers. The map will be kept updated as new content is released. Check out our <a href=\"https://steamcommunity.com/sharedfiles/filedetails/?id=2276192929\">page on Steam</a> for future plans. The work poured into this project is and will continue to be a hobby, if you however wish to show support, I have set up a <a href=\"#\">Flattr page</a> for donations to cover server costs only. We hope our map helps you navigate through the world of Meadow - see you in game!
\t\t\t<div class=\"menu-links\">
\t\t\t\t<span>© Copyright 2020, last updated November 3rd 2020</span>
\t\t\t\t<a href=\"/imprint\">Imprint</a>
\t\t\t\t<a href=\"/privacy\">Privacy</a>
\t\t\t\t<a href=\"#\">Flattr</a>
\t\t\t\t<a href=\"https://steamcommunity.com/sharedfiles/filedetails/?id=2276192929\">Plans</a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-7 menu-filters row\">
\t\t\t<div class=\"single-filters\">
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_inclines.png\" />
\t\t\t\t\t\t<label>Inclines <input type=\"checkbox\" checked data-layer=\"Inclines\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_rises.png\" />
\t\t\t\t\t\t<label>Rises <input type=\"checkbox\" checked data-layer=\"Rises\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"filter\">
\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t<img class=\"filter-icon\" src=\"/img/icon_foliage.png\" />
\t\t\t\t\t\t<label>Catnip and Puffballs <input type=\"checkbox\" checked data-layer=\"Catnip and Puffballs\"></label> <i class=\"filter-check fas fa-check\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t{% for mt in markerTypes %}
\t\t\t\t{% if not mt.parent %}
\t\t\t\t\t<div class=\"filter{% if mt.children|length > 0 %} col filter-parent row{% endif %}\" data-id=\"{{ mt.id }}\" {% if not mt.displayInControls %}style=\"display:none\"{% endif %}>
\t\t\t\t\t\t<div class=\"filter-upper\">
\t\t\t\t\t\t\t<img class=\"filter-icon\" src=\"{{ mt.icon }}\" />
\t\t\t\t\t\t\t<label>{{ mt.title }} <input type=\"checkbox\" {% if mt.defaultshow and not mt.disabled %}checked{% endif %} data-layer=\"{{ mt.title }}\"></label> <i class=\"filter-check fas fa-check\" {% if (not mt.defaultshow) or mt.disabled %}style=\"opacity: 0.3\"{% endif %}></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t{% if mt.children|length > 0 %}
\t\t\t\t\t\t\t{% for child in mt.children %}
\t\t\t\t\t\t\t\t{% if loop.index0 % 6 == 0 %}
\t\t\t\t\t\t\t\t\t<div class=\"col filter-children-group\">
\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t<div class=\"filter filter-child\" data-id=\"{{ child.id }}\" {% if not mt.displayInControls %}style=\"display:none\"{% endif %}>
\t\t\t\t\t\t\t\t\t\t<img class=\"filter-icon\" src=\"{{ child.icon }}\" />
\t\t\t\t\t\t\t\t\t\t<label {% if child.disabled %}style=\"color: #ffc5c5\"{% endif %}>{{ child.title }} <input type=\"checkbox\" {% if child.defaultshow and not child.disabled %}checked{% endif %} data-layer=\"{{ child.title }}\"></label> <i class=\"filter-check fas fa-check\" {% if (not child.defaultshow) or child.disabled %}style=\"opacity: 0.3\"{% endif %}></i>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t{% if loop.index0 % 6 == 5 %}
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t{% endfor %}
\t\t</div>
\t</div>
</div>
<div id=\"markerTypes\">
\t{% for mt in markerTypes %}
\t\t{% if mt.displayInControls %}
\t\t\t<div
\t\t\tdata-id=\"{{ mt.id }}\"
\t\t\tdata-title=\"{{ mt.title }}\"
\t\t\tdata-parent=\"{% if mt.parent %}{{ mt.parent.id }}{% endif %}\"
\t\t\tdata-icon=\"{{ mt.icon }}\"
\t\t\tdata-defaultshow=\"{{ mt.defaultshow }}\"
\t\t\tdata-disabled=\"{{ mt.disabled }}\"
\t\t\t></div>
\t\t{% endif %}
\t{% endfor %}
</div>
<div id=\"markers\">
\t{% for m in markers %}
\t\t{% if m.type.displayInControls %}
\t\t\t<div
\t\t\tdata-type=\"{{ m.type.title }}\"
\t\t\tdata-x=\"{{ m.x }}\"
\t\t\tdata-z=\"{{ m.z }}\"
\t\t\tdata-title=\"{{ m.title }}\"
\t\t\tdata-description=\"{{ m.description }}\"
\t\t\tdata-image=\"{{ m.image }}\"
\t\t\tdata-image-author=\"{{ m.imageAuthor }}\"
\t\t\tdata-icon=\"{% if m.icon %}{{ m.icon }}{% else %}{{ m.type.icon }}{% endif %}\"
\t\t\tdata-icon-width=\"{% if m.iconWidth %}{{ m.iconWidth }}{% elseif m.type.iconWidth %}{{ m.type.iconWidth }}{% elseif m.type.parent.iconWidth %}{{ m.type.parent.iconWidth }}{% else %}50{% endif %}\"
\t\t\tdata-icon-height=\"{% if m.iconHeight %}{{ m.iconHeight }}{% elseif m.type.iconHeight %}{{ m.type.iconHeight }}{% elseif m.type.parent.iconHeight %}{{ m.type.parent.iconHeight }}{% else %}50{% endif %}\"
\t\t\tdata-icon-anchor=\"{% if m.iconAnchor != null %}{{ m.iconAnchor }}{% elseif m.type.iconAnchor != null %}{{ m.type.iconAnchor }}{% elseif m.type.parent and m.type.parent.iconAnchor != null %}{{ m.type.parent.iconAnchor }}{% else %}25{% endif %}\"
\t\t\tdata-disabled=\"{{ m.type.disabled }}\"
\t\t\tdata-underground=\"{{ m.underground }}\"
\t\t\tdata-background=\"{% if m.type.background %}{{ m.type.background }}{% else %}0{% endif %}\"
\t\t\t></div>
\t\t{% endif %}
\t{% endfor %}
</div>
{% endblock %}
", "index.html.twig", "/var/www/meadowmap/templates/index.html.twig");
    }
}
