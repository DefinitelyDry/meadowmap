<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028162431 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE marker_type ADD icon_width INT DEFAULT NULL, ADD icon_height INT DEFAULT NULL, ADD icon_anchor INT DEFAULT NULL, ADD disabled TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE marker ADD icon VARCHAR(255) DEFAULT NULL, ADD icon_width INT DEFAULT NULL, ADD icon_height INT DEFAULT NULL, ADD icon_anchor INT DEFAULT NULL, ADD image_author VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE marker DROP icon, DROP icon_width, DROP icon_height, DROP icon_anchor, DROP image_author');
        $this->addSql('ALTER TABLE marker_type DROP icon_width, DROP icon_height, DROP icon_anchor, DROP disabled');
    }
}
