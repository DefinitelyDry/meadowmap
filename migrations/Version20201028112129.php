<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028112129 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE marker_type (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, icon VARCHAR(255) NOT NULL, display_in_controls TINYINT(1) NOT NULL, INDEX IDX_158B4D84727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE marker (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, x INT NOT NULL, z INT NOT NULL, title VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_82CF20FEC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE marker_type ADD CONSTRAINT FK_158B4D84727ACA70 FOREIGN KEY (parent_id) REFERENCES marker_type (id)');
        $this->addSql('ALTER TABLE marker ADD CONSTRAINT FK_82CF20FEC54C8C93 FOREIGN KEY (type_id) REFERENCES marker_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE marker_type DROP FOREIGN KEY FK_158B4D84727ACA70');
        $this->addSql('ALTER TABLE marker DROP FOREIGN KEY FK_82CF20FEC54C8C93');
        $this->addSql('DROP TABLE marker_type');
        $this->addSql('DROP TABLE marker');
    }
}
