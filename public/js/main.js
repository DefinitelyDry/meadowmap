$(document).ready(function() {
  // Custom Markers
  custom = [];
  
  // Menu Toggle Handler
  $(".togglemenu").click(function () {
    $(".menu-content").slideToggle({start: function () {$(this).css({display: "flex"})}}, function() {

    });
    if($(".togglemenu > i").hasClass('fa-chevron-up')) {
      $(".togglemenu > i").removeClass('fa-chevron-up').addClass('fa-chevron-down');
      $(".togglemenu > span").html("Click here to close filter menu");
    } else {
      $(".togglemenu > i").removeClass('fa-chevron-down').addClass('fa-chevron-up');
      $(".togglemenu > span").html("Click here to filter layers and for more information");
    }
  });

  // General Map Settings
  //var bounds = [[2750,-3250], [-2250,1750]];
  var bounds = [[2000,-4000], [-1500,2500]];
  var map = L.map('map', {
    crs: L.CRS.Simple,
    wheelPxPerZoomLevel: 180,
	zoomDelta: 0.5,
    minZoom: -1.7,
    maxZoom: 0.5,
    center: [0,0],
    maxBounds: bounds,
    maxBoundsViscosity: 1.0
  });

  // Basic Overlay Maps
  var overworld = L.imageOverlay('img/background.jpg', bounds).addTo(map);
  var caves = L.imageOverlay('img/background.jpg', bounds, {opacity: 0.1});
  var textures = L.imageOverlay('img/textures.png', bounds).addTo(map);
  var decorations = L.imageOverlay('img/decorations.png', bounds).addTo(map);
  var mountains = L.imageOverlay('img/mountains.png', bounds).addTo(map);
  var inclines = L.imageOverlay('img/inclines.png', bounds).addTo(map);
  var rises = L.imageOverlay('img/rises.png', bounds).addTo(map);
  var foliage = L.imageOverlay('img/foliage.png', bounds).addTo(map);
  var caves1 = L.imageOverlay('img/caves1.png', bounds, {opacity: 0}).addTo(map);
  var caves2 = L.imageOverlay('img/caves2.png', bounds, {opacity: 0}).addTo(map);
  var caves3 = L.imageOverlay('img/caves3.png', bounds, {opacity: 0}).addTo(map);

  var maps = {
    "Overworld": overworld,
    "Caves": caves
  };

  var layers = {
    "Textures": textures,
    "Decorations": decorations,
    "Mountains": mountains,
    "Inclines": inclines,
    "Rises": rises,
    "Catnip and Puffballs": foliage,
    "Caves (Lower)": caves1,
    "Caves (Middle)": caves2,
    "Caves (Upper)": caves3
  };

  // Marker Types
  var markerTypes = []
  $('#markerTypes').children().each(function() {
    markerTypes[$(this).attr('data-id')] = {
      'title' : $(this).attr('data-title'),
      'parent' : $(this).attr('data-parent'),
      'icon' : $(this).attr('data-icon'),
      'disabled' : $(this).attr('data-disabled'),
      'icon_width' : $(this).attr('data-icon-width'),
      'icon_height' : $(this).attr('data-icon-height'),
      'icon_anchor' : $(this).attr('data-icon-anchor')
    };
    layers[$(this).attr('data-title')] = L.layerGroup();
    if($(this).attr('data-defaultshow') && !$(this).attr('data-disabled')) {
      layers[$(this).attr('data-title')].addTo(map);
    }
  });

  // Markers
  $('#markers').children().each(function() {
    // Setting Popup Anchor for very far east/west markers
    var popupAnchorX = 0;
    if(parseInt($(this).attr('data-x')) > 2100)
      popupAnchorX = 150;
    else if(parseInt($(this).attr('data-x')) < -600)
      popupAnchorX = -160;

    var marker = L.marker(
      [parseInt($(this).attr('data-z')) * -1, parseInt($(this).attr('data-x')) * -1],
      {
        icon: L.icon({
          iconUrl: $(this).attr('data-icon'),
          iconSize: [parseInt($(this).attr('data-icon-width')), parseInt($(this).attr('data-icon-height'))],
          iconAnchor: [parseInt($(this).attr('data-icon-width')) / 2, parseInt($(this).attr('data-icon-anchor'))],
          popupAnchor: [popupAnchorX, (parseInt($(this).attr('data-z')) * -1) > 1200 ? parseInt($(this).attr('data-icon-height')) - parseInt($(this).attr('data-icon-anchor')) : -1 * parseInt($(this).attr('data-icon-anchor'))]
        }),
        zIndexOffset: -1 * parseInt($(this).attr('data-background')),
        cavelayer: $(this).attr('data-underground'),
        opacity: $(this).attr('data-underground') == "0" || $(this).attr('data-type') == "Cave Entrances" ? 1 : 0
      }
    );
    if($(this).attr('data-title')) {
      var popuptext = "<h2 class='marker-title'>" + $(this).attr('data-title') + "</h2>";
      if($(this).attr('data-image'))
        popuptext += "<img src='" + $(this).attr('data-image') + "' class='img-fluid marker-image' />";
      if($(this).attr('data-image-author'))
        popuptext += "<div class='marker-image-author'>Image by " + $(this).attr('data-image-author') + "</div>";
      if($(this).attr('data-description'))
        popuptext += "<div class='marker-description'>" + $(this).attr('data-description') + "</div>";
      if((parseInt($(this).attr('data-z')) * -1) > 1200)
        var popup = L.popup({'className': 'popup-bottom'});
      else var popup = L.popup();
      popup.setContent(popuptext);
      marker.bindPopup(popup);
    }

    // Cave, Stairs events
    if($(this).attr('data-type') == "Cave Entrances")
    	marker.on('click', processCaveEntrance);
    else if($(this).attr('data-type') == "Stairs up")
    	marker.on('click', processStairsUp);
    else if($(this).attr('data-type') == "Stairs down")
    	marker.on('click', processStairsDown);

    layers[$(this).attr('data-type')].addLayer(marker);
  });

  // Final Assembly
  L.control.layers(maps, layers).addTo(map); // Activate this for Overworld/Underground selection
  map.fitBounds(bounds);
  map.panTo([0,0]);
  map.setZoom(0);

  // Event for Caves
  map.on('baselayerchange', processCaves);

  // Refining Filters
  $('.filter').each(function(child) {
    if(!$(this).hasClass('filter-parent') && !$(this).hasClass('filter-child')) {
      $(this).remove();
      $('.single-filters').append($(this));
    }
  });

  // Filter Listeners
  $('.filter-check').click(function() {
    var checkbox = $(this).parent().find('input')[0];
    var checked = $(checkbox).is(':checked');
    if(checked) {
      $(checkbox).prop('checked', false);
      $(this).css('opacity', 0.3);
    }
    else {
      $(checkbox).prop('checked', true);
      $(this).css('opacity', 1);
    }
    changeLayer($(checkbox).attr('data-layer'));
    if($(this).parent().parent().hasClass('filter-parent')) {
      $(this).parent().parent().find('.filter-child').each(function() {
        if(checked == $($(this).find('input')[0]).is(':checked'))
          $($(this).children('.filter-check')[0]).trigger('click');
      });
    }
  });

  $('input:checkbox').change(function() {
    var check = $(this).parent().parent().children('.filter-check')[0];
    var layer = $(this).attr('data-layer');
    var checked = $(this).is(':checked');
    if(checked)
      $(check).css('opacity', 1);
    else $(check).css('opacity', 0.3);
    changeLayer(layer);
    if($(this).parent().parent().parent().hasClass('filter-parent')) {
      $(this).parent().parent().parent().find('.filter-child').each(function() {
        if(checked != $($(this).find('input')[0]).is(':checked'))
          $($(this).children('.filter-check')[0]).trigger('click');
      });
    }
  });

  // Marker Type Selection
  $('#markertypeselect').on('change', function() {
    var mt = markerTypes[this.value];
    $('#markertypeicon').val(mt['icon']);
    $('#markertypeiconwidth').attr('placeholder', mt['icon_width']);
    $('#markertypeiconheight').attr('placeholder', mt['icon_height']);
    $('#markertypeiconanchor').attr('placeholder', mt['icon_anchor']);
  });

  // Changes Layer in Map Application
  function changeLayer(title) {
    $('.leaflet-control-layers-selector').each(function() {
      if($($(this).parent().children('span')[0]).html() == (" " + title))
        $(this).trigger('click');
    });
  }

  // Marker Sizes on Zooming
  map.on('zoomend', function() {
  	var currentZoom = map.getZoom();
  	for (var key in layers) {
  		if(!layers[key]._url) {
	  		layers[key].eachLayer(function(layer) {
	  			switch(currentZoom) {
	  				case 0.5:
	  					scale = 1.5;
	  					break;
	  				case -0.5:
	  					scale = 0.75;
	  					break;
	  				case -1:
	  					scale = 0.5;
	  					break;
	  				case -1.7:
	  					scale = 0.33;
	  					break;
	  				default:
	  					scale = 1;
	  					break;
	  			}
	  			var oldicon = layer.options.icon;
	  			if(!(layer.initialsize && layer.initialanchor)) {
	  				layer.initialsize = oldicon.options.iconSize;
	  				layer.initialanchor = oldicon.options.iconAnchor;
	  			}
				var newicon = L.icon({
		          iconUrl: oldicon.options.iconUrl,
		          iconSize: [layer.initialsize[0] * scale, layer.initialsize[1] * scale],
		          iconAnchor: [layer.initialanchor[0] * scale, layer.initialanchor[1] * scale],
		          popupAnchor: oldicon.options.popupAnchor
		        });
		        layer.setIcon(newicon);
			});
		}
  	}
  });

  // Sidemenu: Marker
  $('.btn-marker-actualbutton').click(function() {
    if($('.marker-subcontent').is(':visible')) {
      $('.marker-subcontent').hide();
      $('#sidemenu-marker').css('top', '-140px');
      $('#map').css('cursor', 'grab');
      $('.btn-marker-text').html('Add marker');
    } else {
      $('.marker-subcontent').show();
      $('#sidemenu-marker').css('top', '-290px');
	  $('#map').css('cursor', 'default');
      $('.btn-marker-text').html('Stop adding markers');
    }
  });

  // Changing Marker Color
  $('.marker-color-button').click(function() {
  	$('.marker-color-button.active').removeClass('active');
  	$(this).addClass('active');
  });

  // Inserting Marker from Button Click
  $('.btn-insert').click(function() {
    var coordinates = $('.input-coordinates').val().replace('(', '').replace(')', '').split(',');
    // Checking range
    if(coordinates.length == 3 && coordinates[0] >= -1500 && coordinates[0] <= 2000 && coordinates[2] >= -2500 && coordinates[2] <= 1000) {
      var x = coordinates[0];
      var z = coordinates[2];
      var marker = L.marker(
        [z * -1, x * -1],
        {
          icon: L.icon({
            iconUrl: "img/marker-" + $('.marker-color-button.active').attr('data-color') + ".png",
            iconSize: [20, 35],
            iconAnchor: [10, 35]
          })
        }
      ).addTo(map);
      custom.push(marker);
    }
  });

  // Inserting Marker from Map Click
  map.on('click', function(e) {
    if($('.marker-subcontent').is(':visible')) {
      var marker = L.marker(
        [e.latlng.lat, e.latlng.lng],
        {
          icon: L.icon({
            iconUrl: "img/marker-" + $('.marker-color-button.active').attr('data-color') + ".png",
            iconSize: [20, 35],
            iconAnchor: [10, 35]
          })
        }
      ).addTo(map);
      custom.push(marker);
    }
  });

  // Clear all existing markers
  $('.btn-clear').click(function() {
    for(var i = 0; i < custom.length; i++)
      map.removeLayer(custom[i]);
    custom = [];
  });

  // Exporting
  $('.btn-export').click(async function() {
    $('#loading-overlay').show();
    map.setZoom(0);
    map.panTo([250,-750]);
    await sleep(1000);
    await printMap();
    await sleep(15000);
    $('#loading-overlay').hide();
  })

  function sleep(milliseconds) {
   return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  async function printMap() {
    var fullSize = {
      width: 3500,
      height: 3500,
      className: 'fullSizeClass',
      tooltip: 'Full size',
      target: {
        className: 'fullSizeClass'
      }
    }
    var printPlugin = L.easyPrint({
      hidden: true,
      sizeModes: [fullSize],
      exportOnly: true,
      filename: 'map'
    }).addTo(map); 
    printPlugin.printMap(fullSize, 'map');
    return;
  }

  // Cave and Stairs functions
  function processCaves(e) {
  	var defaultClassName = "leaflet-marker-icon leaflet-zoom-hide leaflet-interactive";
  	var hiddenClassName = "leaflet-marker-icon leaflet-zoom-hide leaflet-interactive hidden";
  	if(e.name == "Caves") {
  		for(var key in layers) {
  			if(layers[key]._url)
  				layers[key].setOpacity(0.1);
	  		else {
	  			layers[key].eachLayer(function(layer) {
	  				if(layer.options.cavelayer == "1") {
	  					if(layer._icon)
	  						layer._icon.className = defaultClassName;
	  					layer.setOpacity(1);
	  				}
					else {
						if(layer._icon)
							layer._icon.className = hiddenClassName;
						layer.setOpacity(0);
					}
				});
	  		}
	  		if(key == "Caves (Lower)")
	  			layers[key].setOpacity(1);
	  	}
  	} else if(e.name == "Caves2") {
  		for(var key in layers) {
  			if(layers[key]._url)
  				layers[key].setOpacity(0.1);
	  		else {
	  			layers[key].eachLayer(function(layer) {
	  				if(layer.options.cavelayer == "2") {
	  					if(layer._icon)
	  						layer._icon.className = defaultClassName;
	  					layer.setOpacity(1);
	  				}
					else {
						if(layer._icon)
							layer._icon.className = hiddenClassName;
						layer.setOpacity(0);
					}
				});
	  		}
	  		if(key == "Caves (Middle)")
	  			layers[key].setOpacity(1);
	  	}
  	} else if(e.name == "Caves3") {
  		for(var key in layers) {
  			if(layers[key]._url)
  				layers[key].setOpacity(0.1);
	  		else {
	  			layers[key].eachLayer(function(layer) {
	  				if(layer.options.cavelayer == "3") {
						if(layer._icon)
							layer._icon.className = defaultClassName;
						layer.setOpacity(1);
					}
					else {
						if(layer._icon)
							layer._icon.className = hiddenClassName;
						layer.setOpacity(0);
					}
				});
	  		}
	  		if(key == "Caves (Upper)")
	  			layers[key].setOpacity(1);
	  	}
	} else {
  		for(var key in layers) {
  			if(layers[key]._url)
  				layers[key].setOpacity(1);
	  		else {
	  			layers[key].eachLayer(function(layer) {
					if(layer.options.cavelayer == "0" || key == "Cave Entrances") {
	  					if(layer._icon)
							layer._icon.className = defaultClassName;
						layer.setOpacity(1);
					}
					else {
						if(layer._icon)
							layer._icon.className = hiddenClassName;
						layer.setOpacity(0);
					}
				});
	  		}
	  		if(key.indexOf("Caves") != -1)
	  			layers[key].setOpacity(0);
  		}
  	}
  }

  function processCaveEntrance(e) {
  	if(layers["Decorations"].options.opacity == 0.1) {
  		map.removeLayer(caves);
  		map.addLayer(overworld);
  		processCaves({name: ""});
  	}
  	else {
  		map.removeLayer(overworld);
  		map.addLayer(caves);
	  	switch(e.target.options.cavelayer) {
	  		case "1":
	  			processCaves({name: "Caves"});
	  			break;
	  		case "2":
	  			processCaves({name: "Caves2"});
	  			break;
	  		case "3":
	  			processCaves({name: "Caves3"});
	  			break;
	  	}
	}
  }

  function processStairsUp(e) {
  	// Lower -> Middle
  	if(layers["Caves (Lower)"].options.opacity == 1) {
  		processCaves({name: "Caves2"});
  	}
  	// Middle -> Upper
  	else if(layers["Caves (Middle)"].options.opacity == 1) {
  		processCaves({name: "Caves3"});
  	}
  }

  function processStairsDown(e) {
  	// Middle -> Lower
  	if(layers["Caves (Middle)"].options.opacity == 1) {
  		processCaves({name: "Caves"});
  	}
  	// Middle -> Upper
  	else if(layers["Caves (Upper)"].options.opacity == 1) {
  		processCaves({name: "Caves2"});
  	}
  }
});